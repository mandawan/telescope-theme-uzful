Package.describe({
    summary: "Telescope Uzful theme",
    version: '0.1.0',
    name: "telescope-theme-uzful"
});

Npm.depends({
  "html-to-text": "0.1.0"
});

Package.onUse(function (api) {
    api.use(['templating', 'telescope:theme-base', 'telescope:theme-hubble', 'fourseven:scss'], ['client']);

    api.use([
        // core dependencies
        'templating',
        'telescope:core@0.1.0',
        'telescope:theme-hubble@0.1.0',
        'benjaminrh:jquery-cookie',
        'miro:mailchimp@1.0.4',
        'telescope:newsletter',
  ]);

    api.addFiles([
        'lib/client/uzful.js',
        'lib/custom_routes.js',
        'lib/custom_permissions.js',
        'lib/posts/custom_methods.js',
        'lib/custom_utils.js',
        'lib/client/scripts/custom_categories.js',
        "i18n/fr.i18n.json",
    ], ['client', 'server']);

    api.addFiles([
        'lib/client/scss/screen.scss',
        'lib/client/templates/new_posts_list.html',
        'lib/client/templates/week_posts.html',
        'lib/client/templates/update_post_upvote.html',
        'lib/client/templates/update_post_content.html',
        'lib/client/templates/new_filter_week.html',
        'lib/client/templates/new_nav.html',
        'lib/client/templates/new_single_week_nav.html',
        'lib/client/templates/new_newsletter_banner.html',
        'lib/client/templates/new_layout.html',
        'lib/client/templates/new_post_item.html',
        'lib/client/templates/custom_post_admin.js',
        'lib/client/templates/new_categorie_nav.html',

        'public/img/circle.png',
        'public/fonts/icomoon.eot',
        'public/fonts/icomoon.svg',
        'public/fonts/icomoon.ttf',
        'public/fonts/icomoon.woff',
        'public/fonts/SourceSans/opensans.ttf',
        'public/fonts/Boton/BotonCFFLig.otf',

        'lib/client/scripts/templates.js',
        'lib/client/templates/custom_post_item.js',
  ], ['client']);

    api.addFiles([
        'lib/server/custom_campaign.js',
        'lib/server/custom_cron.js',
        'lib/server/custom_routes.js',
        'lib/server/custom_mailchimp.js',
        'lib/server/new_posts.js',
        'lib/server/custom_methods.js',
        
        'lib/server/templates/newsletter/custom_emailDigest.handlebars',
        'lib/server/templates/newsletter/custom_emailPostItem.handlebars',
        'lib/server/templates/newsletter/custom_emailWrapper.handlebars',
    ], ['server']);
});