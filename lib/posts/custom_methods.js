//Add new system for posts

/**
 * Insert a post in the database (note: optional post properties not listed here)
 * @param {Object} post - the post being inserted
 * @param {string} post.userId - the id of the user the post belongs to
 * @param {string} post.title - the post's title
 */
Posts.submit = function (post) {

  var userId = post.userId, // at this stage, a userId is expected
      user = Users.findOne(userId);

  // ------------------------------ Checks ------------------------------ //

  // check that a title was provided
  if(!post.title)
    throw new Meteor.Error(602, i18n.t('please_fill_in_a_title'));

  // check that there are no posts with the same URL
  if(!!post.url)
    Posts.checkForSameUrl(post.url, user);

  // ------------------------------ Properties ------------------------------ //

  var defaultProperties = {
    createdAt: new Date(),
    author: Users.getDisplayNameById(userId),
    upvotes: 0,
    downvotes: 0,
    commentCount: 0,
    clickCount: 0,
    viewCount: 0,
    baseScore: 0,
    score: 0,
    inactive: false,
    sticky: false,
    status: Posts.getDefaultStatus(),
    postedAt: new Date()
  };

  post = _.extend(defaultProperties, post);

  // if post is approved but doesn't have a postedAt date, give it a default date
  // note: pending posts get their postedAt date only once theyre approved
  if (post.status === Posts.config.STATUS_APPROVED && !post.postedAt)
    post.postedAt = new Date();

  // clean up post title
  post.title = Telescope.utils.cleanUp(post.title);

  // ------------------------------ Callbacks ------------------------------ //

  // run all post submit server callbacks on post object successively
  post = Telescope.callbacks.run("postSubmit", post);

  // -------------------------------- Insert ------------------------------- //

  post._id = Posts.insert(post);

  // --------------------- Server-Side Async Callbacks --------------------- //

  Telescope.callbacks.runAsync("postSubmitAsync", post);

  return post;
};

//Status 4 is approveAndWaitPost
Meteor.methods({
      approveAndWaitPost: function(post){
        if(Users.is.admin(Meteor.user())){
          var set = {status: 4};

          // unless post is already scheduled and has a postedAt date, set its postedAt date to now
          if (!post.postedAt)
            set.postedAt = new Date();

          Posts.update(post._id, {$set: set}, {validate: false});

          Telescope.callbacks.runAsync("postApprovedAsync", post);

        }else{
          Messages.flash('You need to be an admin to do that.', "error");
        }
      },
    rejectedPost: function(post){
        if(Users.is.admin(Meteor.user())){
          var set = {status: 3};

          Posts.update(post._id, {$set: set}, {validate: false});

          Telescope.callbacks.runAsync("postApprovedAsync", post);

        }else{
          Messages.flash('You need to be an admin to do that.', "error");
        }
    }
});