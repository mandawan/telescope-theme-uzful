Meteor.startup(function () {

  Router.route('/email/custom_campaign', {
    name: 'custom_campaign',
    where: 'server',
    action: function() {
      var campaign = buildCampaign(getCampaignPosts(Settings.get('postsPerNewsletter', 5)));
      var campaignSubject = '<div class="campaign-subject"><strong>Subject:</strong> '+campaign.subject+' (note: contents might change)</div>';
      var campaignSchedule = '<div class="campaign-schedule"><strong>Scheduled for:</strong> '+ Meteor.call('getNextJobCustom') +'</div>';

      this.response.write(campaignSubject+campaignSchedule+campaign.html);
      this.response.end();
    }
  });

});
