/*
    Major change:
        - Add a schedule (fridays)
        - Add in cron system category to send
*/

var defaultFrequency = 7; // once a week
var defaultTime = '00:00';

var getSchedule = function (parser, add) {
    var frequency = Settings.get('newsletterFrequency', defaultFrequency);
    var recur = parser.recur();
    var schedule;
    switch (frequency) {
    case 1: // every day
        // sched = {schedules: [{dw: [1,2,3,4,5,6,0]}]};
        schedule = recur.on(1, 2, 3, 4, 5, 6, 0).dayOfWeek();
        break;

    case 2: // Mondays, Wednesdays, Fridays
        // sched = {schedules: [{dw: [2,4,6]}]};
        schedule = recur.on(2, 4, 6).dayOfWeek();
        break;

    case 3: // Mondays, Thursdays
        // sched = {schedules: [{dw: [2,5]}]};
        schedule = recur.on(2, 5).dayOfWeek();
        break;

    case 4: // Fridays
        schedule = recur.on(6).dayOfWeek();
        break;

    case 7: // Once a week (Mondays)
        // sched = {schedules: [{dw: [2]}]};
        schedule = recur.on(2).dayOfWeek();
        break;

    default: // Once a week (Mondays)
        schedule = recur.on(2).dayOfWeek();
    }

    var time = Settings.get('newsletterTime');
    console.log(time);
    if(time){
        time = time.split(":").map(function(item) {
            return parseInt(item, 10);
        });
        console.log("Newsletter panel", Settings.get('newsletterTime'));
        console.log("Config to UTC", time[0]-2+":"+ (time[1]+add));
        return schedule.on(time[0]-2+":"+(time[1]+add)).time();
    }else{
        return schedule.on(Settings.get('newsletterTime', defaultTime)).time();
    }
};

Meteor.methods({
  getNextJobCustom: function () {
    var nextJob = SyncedCron.nextScheduledAtDate('scheduleNewsletters');
    console.log(nextJob);
    return nextJob;
  }
});

var addJobCustom = function () {
    SyncedCron.add({
        name: 'scheduleNewsletters',
        schedule: function (parser) {
            // parser is a later.parse object
            console.log("Newsletter cron");
            return getSchedule(parser, 1);
        },
        job: function () {
            console.log("Let's goo !");
            scheduleNextCampaign(false);
        }
    });
    
    SyncedCron.add({
        name: 'displayPosts',
        schedule: function (parser) {
            // parser is a later.parse object
            console.log("displayPosts cron");
            return getSchedule(parser, 0);
        },
        job: function () {
            console.log("Display Post Best");
            displayPostsBest();
        }
    });
    
    SyncedCron.add({
        name: 'PendingPostsToTommorow',
        schedule: function (parser) {
            // parser is a later.parse object
            console.log("PendingPostsToTommorow cron");
            return parser.recur().on(1, 2, 3, 4, 5, 6, 0).dayOfWeek().on("23:58").time();
        },
        job: function () {
            console.log("Pending Posts To Tommorow");
            displayPendingTom();
        }
    });
};

Meteor.startup(function () {
    console.log(Settings.get('enableNewsletter', true));
    if (Settings.get('enableNewsletter', true)) {
        console.log("add job custom");
        addJobCustom();
    }
});
