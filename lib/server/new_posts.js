/*Function which will put the posts in approved*/

displayPostsBest = function () {
    console.log("Begin Displays");
    Posts.find({
        //MODIFMAX END
        /*postedAt:  {
                $gte: new Date(moment().format('YYYY-MM-DD'))
            }*/
        postedAt: {
                $gte: new Date(moment().day(1).format('YYYY-MM-DD')),
                $lte:  new Date(moment().day(5).format('YYYY-MM-DD'))
            }
    }, {
        sort: {
            postedAt: -1
        }
    }).forEach(function (post) {
        console.log("Each post");
        var set = {
            status: 2
        };
        
        if (post.status == 4) {
            console.log("Status 4 to 2");
            // unless post is already scheduled and has a postedAt date, set its postedAt date to now
            if (!post.postedAt)
                set.postedAt = new Date();

            Posts.update(post._id, {
                $set: set
            }, {
                validate: false
            });

        }

        Telescope.callbacks.runAsync("postApprovedAsync", post);

    });
};

displayPendingTom = function (){
    console.log("Begin Pending", new Date(moment().format('YYYY-MM-DD')));
    Posts.find({
        //MODIFMAX END
        /*postedAt:  {
                $gte: new Date(moment().format('YYYY-MM-DD'))
            }*/
        postedAt: {
                $gte: new Date(moment().day(1).format('YYYY-MM-DD')),
                $lte:  new Date(moment().day(5).format('YYYY-MM-DD'))
            }
    }, {
        sort: {
            postedAt: -1
        }
    }).forEach(function (post) {
        console.log("Each post");
        
        if (post.status == 1) {
            console.log('status 1', post._id, new Date(moment().add(1, 'days').format('YYYY-MM-DD')));
            var postedAt = new Date(moment().add(1, 'days').format('YYYY-MM-DD'));
            Posts.update(post._id, {
                $set: {
                    postedAt: postedAt
                }
            });
        } 
    });
}