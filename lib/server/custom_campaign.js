/*
    Major change:
        - Change day to week
        - Add system category for newsletter
*/
defaultFrequency = 7;
defaultPosts = 3;

getCampaignPosts = function (postsCount, category) {

    // look for last scheduled campaign in the database
    var lastCampaign = SyncedCron._collection.findOne({
        name: 'Schedule newsletter'
    }, {
        sort: {
            finishedAt: -1
        },
        limit: 1
    });

    // if there is a last campaign use its date, else default to posts from the last 7 days
    var lastWeek = /*moment().startOf('day').toDate()*/ moment().subtract(7, 'days').toDate(); // MODIFMAX END
    var after = (typeof lastCampaign !== 'undefined') ? lastCampaign.finishedAt : lastWeek
    console.log("##### Category #####",category);
    var params = Posts.getSubParams({
        view: 'campaign',
        limit: postsCount,
        after: after,
        category: category
    });
    
    params.find.postedAt = {
        $gte: new Date(moment().format('YYYY-MM-DD'))
    }
    params.find.status = 2;
    
    //console.log("##### POSTS #####", Posts.find(params.find, params.options).fetch());
    console.log("##### POSTS #####", params.find);
    return Posts.find(params.find, params.options).fetch();
};

buildCampaign = function (postsArray, category) {
    var postsHTML = '',
        subject = '';
    // 1. Iterate through posts and pass each of them through a handlebars template
    postsArray.forEach(function (post, index) {
        /*if (index > 0)
            subject += ', ';*/
        console.log("##########", postsArray.length, post, index, "##########");
        subject = "édition " + category;
        /*subject += " ";
        subject += moment(post.postedAt).format("MMMM D YYYY");*/

        var postUser = Meteor.users.findOne(post.userId);

        // the naked post object as stored in the database is missing a few properties, so let's add them
        var properties = _.extend(post, {
            authorName: Users.getDisplayName(post),
            postLink: Posts.getLink(post),
            profileUrl: Users.getProfileUrl(postUser),
            postPageLink:  Posts.getPageUrl(post),
            date: moment(post.postedAt).format("MMMM D YYYY"),
            
        });

        console.log("##########", properties, "##########");
        if (post.body)
            properties.body = marked(Telescope.utils.trimWords(post.body, 20)).replace('<p>', '').replace('</p>', ''); // remove p tags

        if (post.url)
            properties.domain = Telescope.utils.getDomain(post.url);

        postsHTML += Telescope.email.getTemplate('emailPostItem')(properties);
    });

    console.log("### postsHTML: ",postsHTML,"######");
    
    // 2. Wrap posts HTML in digest template
    var digestHTML = Telescope.email.getTemplate('emailDigest')({
        siteName: Settings.get('title'),
        category: category,
        date: moment().lang("fr").startOf('week').format("dddd, MMMM Do YYYY"),
        content: postsHTML
/*        site: window.location.origin*/
    });
    
    console.log("### digest: ",digestHTML,"######");

    // 3. wrap digest HTML in email wrapper template
    var emailHTML = Telescope.email.buildTemplate(digestHTML);

    var campaign = {
        postIds: _.pluck(postsArray, '_id'),
        subject: Telescope.utils.trimWords(subject, 15),
        html: emailHTML
    };

    console.log("##### Campaign #####", campaign);
    return campaign;
};



scheduleNextCampaign = function (isTest) {
    isTest = !!isTest;
    console.log("isTest ?", isTest);
    //For each category, build a campaign list for newsletter
    Categories.find().forEach(function (category) {
    var posts = getCampaignPosts(Settings.get('postsPerNewsletter', defaultPosts), category.slug);
        console.log(posts.length);
        if (posts.length >= 3) {
            console.log("##### It's 3 in ", category.name, category.listId);
            //return "Ok schedule campaign";
            return scheduleCampaign(buildCampaign(posts, category.name), isTest, category.listId);
        } else {
            var result = 'Not enough posts to schedule today…';
            return result;
        }
    });
};

Meteor.methods({
    testaCampaign: function () {
        // if (Users.is.adminById(this.userId))
            return scheduleNextCampaign(true);
    }
});
