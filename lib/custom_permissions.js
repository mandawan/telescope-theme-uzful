Users.can.viewPendingPosts = function (user) {
  return true;
};

Users.can.viewRejectedPosts = function (user) {
  return true;
};

Users.can.comment = function (user, returnError) {
  return true;
};