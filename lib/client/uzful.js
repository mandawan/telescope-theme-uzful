/* AccountsTemplates.configure({
   texts: {
     socialIcons: {
       facebook: "icon ion-social-facebook",
       twitter: "icon ion-social-twitter",
       meteor: "icon ion-planet"
     }
   }
 });

 themeSettings.useDropdowns = false;

 primaryNav = ['viewsMenu'];
 secondaryNav = ['notificationsMenu', 'userMenu'];

 viewNav = [
   {
     route: 'posts_top',
     label: 'Trending'
   },
   {
     route: 'posts_new',
     label: 'Recent'
   }
 ];

 postModules = [
   {
     template: 'postUpvote',
     order: 10
   },
   {
     template: 'postContent',
     order: 20
   },
   {
     template: 'postAvatars',
     order: 30
   },
   {
     template: 'postDiscuss',
     order: 40
   },
   {
     template: 'postActions',
     order: 50
   }
 ];

 postThumbnail = [];

 postHeading = [
   {
     template: 'postTitle',
     order: 10
   }
 ];

 postMeta = [
   {
     template: 'postDomain',
     order: 10
   },
   {
     template: 'postInfo',
     order: 20
   },
   {
     template: 'postAdmin',
     order: 30
   }
 ];

 userProfileDisplay = [
   {
     template: 'craterUserInfo',
     order: 1
   },
   {
     template: 'craterUserPosts',
     order: 2
   },
   {
     template: 'craterUserUpvotedPosts',
     order: 3
   },
   {
     template: 'craterUserDownvotedPosts',
     order: 4
   },
   {
     template: 'craterUserComments',
     order: 5
   }
 ];

 templates['layout']               = 'craterLayout';
 templates['footer']               = 'craterFooter';
 templates['nav']                  = 'craterNav';
 templates['search']               = 'craterSearch';
 templates['notificationsMenu']    = 'craterNotificationsMenu';
 templates['userMenu']             = 'craterUserMenu';
 templates['categoriesMenu']       = 'craterCategoriesMenu';
 templates['adminMenu']            = 'craterAdminMenu';
 templates['comment_item']         = 'craterCommentItem';
 templates['postAdmin']            = 'craterPostAdmin';
 templates['postAvatars']          = 'craterPostAvatars';
 templates['postCategories']       = 'craterPostCategories';
 templates['postDiscuss']          = 'craterPostDiscuss';
 templates['postDomain']           = 'craterPostDomain';
 templates['postInfo']             = 'craterPostInfo';
 templates['postUpvote']           = 'craterPostUpvote';
 templates['post_body']            = 'craterPostBody';
 templates['post_edit']            = 'craterPostEdit';
 templates['postsLoadMore']        = 'craterPostsLoadMore';
 templates['postSubscribe']        = 'craterPostSubscribe';
 templates['user_profile']         = 'craterUserProfile';
 templates['userComments']         = 'craterUserComments';
 templates['userDownvotedPosts']   = 'craterUserDownvotedPosts';
 templates['userInfo']             = 'craterUserInfo';
 templates['userPosts']            = 'craterUserPosts';
 templates['userUpPosts']          = 'craterUserUpPosts';*/

AccountsTemplates.configureRoute('signUp', {
    path: '/register',
    template: 'new_register'
});

/*Add fridays for newsletter*/
Settings.addField([

    {
        fieldName: 'newsletterFrequency',
        fieldSchema: {
            type: Number,
            optional: true,
            autoform: {
                group: 'newsletter',
                instructions: 'Defaults to once a week. Changes require restarting your app to take effect.',
                options: [
                    {
                        value: 1,
                        label: 'Every Day'
          },
                    {
                        value: 2,
                        label: 'Mondays, Wednesdays, Fridays'
          },
                    {
                        value: 3,
                        label: 'Mondays & Thursdays'
          },
                    {
                        value: 4,
                        label: 'Fridays'
          },
                    {
                        value: 7,
                        label: 'Once a week (Mondays)'
          }
        ]
            }
        }
  }
]);

/**
 * Base parameters that will be common to all other view unless specific properties are overwritten
 */
Posts.views.baseParameters = {
  find: {
    
  },
  options: {
    limit: 10
  }
};


//Change optional in false, so the categories is require

Posts.addField({
    fieldName: 'categories',
    fieldSchema: {
        type: [String],
        optional: false,
        editableBy: ["member", "admin"],
        autoform: {
            noselect: true,
            options: function () {
                var categories = Categories.find().map(function (category) {
                    return {
                        value: category._id,
                        label: category.name
                    };
                });
                return categories;
            }
        }
    }
});

/*This is a test*/
Posts.views.add("userPosts2", function (terms) {
    return {
        find: {
            userId: terms.userId
        },
        options: {
            limit: 5,
            sort: {
                postedAt: -1
            }
        }
    };
});


Posts.views.add("pastWeek", function (terms) {
    return {
        find: {
            createdAt: { /*$gte: terms.weekAgoDate, */
                $lt: moment().format() /*terms.today*/
            }
        },
        options: {
            limit: terms.limit,
            sort: {
                postedAt: -1
            }
        }
    };
});

Posts.views.add("singleWeek", function (terms) {
    return {
        find: {
            postedAt: {
                $gte: terms.after,
                $lt: terms.before
            }
        },
        options: {
            sort: {
                sticky: -1,
                score: -1
            }
        }
    };
});

var getDefaultViewController = function () {
  var defaultView = Settings.get('defaultView', 'top');
  // if view we got from settings is available in Posts.views object, use it
  if (!!Posts.controllers[defaultView]) {
    return Posts.controllers.singleweek;
  } else {
    return Posts.controllers.top;
  }
};

Posts.controllers.singleweek = Posts.controllers.list.extend({

    view: 'singleWeek',

    template: 'single_day', // use single_day template to get prev/next day navigation

    onBeforeAction: function () {
        this.render('category_title', {
            to: 'postsListTop'
        });
        this.next();
    },


    data: function () {
        var today = moment();
        var daysToFriday = 0 - (1 - today.isoWeekday()) + 3;
        var lastWeek = /*today;*/today.subtract('days', daysToFriday);//MODIFMAX 1 to daysToFriday END
        var currentDate = this.params.day ? new Date(this.params.year, this.params.month - 1, this.params.day) : lastWeek;
        if(!Meteor.user()){
            var lastWeek = today.subtract(1, 'weeks');//MODIFMAX days to weeks END
            var currentDate = this.params.day ? new Date(this.params.year, this.params.month - 1, this.params.day) : lastWeek;
            var terms = {
                view: 'singleWeek',
                date: currentDate,
                after: moment(currentDate).toDate(),
                before: moment(currentDate).add(7, 'days').toDate(),
                //MODIFMAX END
                /*after: moment(currentDate).startOf('day').toDate(),
                before: moment(currentDate).endOf('day').toDate(),*/
                category: this.params.slug
            };
        }else{
            var terms = {
                view: 'singleWeek',
                date: currentDate,
                after: moment(currentDate).toDate(),
                before: moment(currentDate).add(7, 'days').toDate(),
                //MODIFMAX END
                /*after: moment(currentDate).startOf('day').toDate(),
                before: moment(currentDate).endOf('day').toDate(),*/
                category: this.params.slug
            };
        }
        return {
            terms: terms
        };
    },

    getCurrentCategory: function () {
        return Categories.findOne({
            slug: this.params.slug
        });
    },

    getTitle: function () {
        return this.getCurrentCategory().name;
    },

    getDescription: function () {
        return this.getCurrentCategory().description;
    }

});


/**
 * Get default status for new posts.
 * @param {Object} user
 */
Posts.getDefaultStatus = function (user) {
    return Posts.config.STATUS_PENDING
};

Meteor.startup(function () {

    Router.route('/category/:slug/week/:year/:month/:day', {
        name: 'postsSingleWeek',
        controller: Posts.controllers.singleweek
    });
    
    
    
    Router.route('/registernewusersuzful', {
        name: 'rbim',
        template: 'registerUser'
    });
    
    Router.route('/week/:year/:month/:day', {
        name: 'postsSingleAllWeek',
        controller: Posts.controllers.singleweek
    });

});
Router.route('/category/:slug', {
    name: 'posts_category_week',
    controller: Posts.controllers.singleweek,
    onAfterAction: function () {
        this.slug = this.params.slug;
        Session.set('categorySlug', this.params.slug);
    }
});

Router.route('/pastWeek', {
    controller: Telescope.controllers.pastWeek,
    name: 'weekPosts'
});

//Modules for nav

/*Telescope.modules.add("primaryNewNav", {
  template: 'categories_menu',
  order: 60
});*/

Telescope.modules.add("primaryNewNav", {
  template: 'search',
  order: 100
});
Telescope.modules.add("secondaryNav", {
  template: 'categories_menu',
  order: 1
});

//Modules for footer
Telescope.modules.add("primaryFooter", {
  template: "pages_new_menu",
    order: 1
});
