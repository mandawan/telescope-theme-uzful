/* Setup template */

// Template.weekPosts = "week_posts";
Template.newPostsList.replaces('posts_list');
Template.updatePostUpvote.replaces('post_vote');
Template.update_post_content.replaces('post_content');
Template.single_week_nav.replaces('single_day_nav');
Template.new_post_avatars.replaces('post_avatars');
Template.new_post_discuss.replaces('post_discuss');
Template.new_nav.replaces('nav');
Template.new_newsletter_banner.replaces('newsletter_banner');
Template.new_layout.replaces('layout');
Template.new_post_item.replaces('post_item');
Template.update_post_info.replaces('post_info');
Template.update_post_comments_link.replaces('post_comments_link');

Template.update_user_menu.replaces('user_menu');
Template.update_post_page.replaces('post_page');
Template.new_logo.replaces('logo');
Template.update_post_admin.replaces('post_admin');
Template.weekPosts.replaces('single_day');
Template.new_categories_menu.replaces('categories_menu');
Template.update_post_author.replaces('post_author');
Template.newPostsLoadMore.replaces('postsLoadMore');


Template.pages_new_menu.helpers({
    hasPages: function () {
        /* console.log(Pages.find().count());*/
        return Pages.find().count();
    },
    pages: function () {
        /*console.log(Pages.find());*/
        return Pages.find();
    }
});

// TODO MODIFY
Template.weekPosts.helpers({
    arguments: function () {
        var today = moment();
        today.format();
        //MODIFMAX END
        var weekAgoDate = moment().subtract("days", 7).format(); // same as today.add("days", -7)
        //var weekAgoDate = moment().subtract("days", 1).format(); // same as today.add("days", -7)
        return {
            terms: {
                view: 'pastWeek',
                weekAgoDate: weekAgoDate,
                today: today,
                limit: 3
            }
        }
    }
});



Template.posts_list.helpers({
    //Limit best post
    topPosts: function () {
        var posts = Posts.find({
            status: 2
        }, {
            sort: {
                upvotes: -1
            },
            limit: 3
        });
        if(posts)
            $(".no-posts").remove();
        return posts;
    },
    //Check if is approve
    showApprove: function () {
        return this.status === Posts.config.STATUS_APPROVED || this.status === 4;
    },
    // Pending
    showUnapprove: function () {
        return this.status === Posts.config.STATUS_PENDING;
    },
    // Rejected
    showRejected: function () {
        return this.status === Posts.config.STATUS_REJECTED;
    },
    // test if has posts
    test: function () {
        if (this.postsCursor) { // not sure why this should ever be undefined, but it can apparently
            var posts = this.postsCursor.map(function (post, index) {
                post.rank = index;
                return post;
            });
            if (posts.length >= 1) {
                $(".no-posts").remove();
                return true;
            } else {
                return false;
            }
        }
    },
    ifCheck: function (a) {
        if ($("div." + a).children(".post").length > 0) {
            $("div." + a + "> .noPost").remove();
        } else {
            $("div." + a + "> .noPost").remove();
            $("div." + a).append("<div class='noPost'>Aucun post dans cette section</div>");
        }
    }
});


//Small description under the title
Template.post_content.helpers({
    descrip: function () {
        var elements = $(this.htmlBody);

        var text = elements[0].innerHTML.substring(0, 120);
        text += "...";
        return text;

    },
});


//System filter per weeks
var getDateURL = function (moment, category) {
    if (typeof category == "undefined") {
        return Router.path('postsSingleAllWeek', {
            year: moment.year(),
            month: moment.month() + 1,
            day: moment.date()
        });
    } else {
        return Router.path('postsSingleWeek', {
            slug: category,
            year: moment.year(),
            month: moment.month() + 1,
            day: moment.date()
        });
    }
};

Template.single_day_nav.onCreated(function () {
    $(document).unbind('keyup'); //remove any potential existing bindings to avoid duplicates
});

//Change date in week
Template.single_week_nav.helpers({
    // week to day
    currentDate: function () {
        var currentDate = moment(this.terms.date);
        var today = moment(new Date());
        var diff = today.diff(currentDate, 'weeks');//MODIFMAX END weeks
        //console.log(currentDate.format("dddd Do MMMM"));
        
        return currentDate.format("dddd Do MMM");
    },
    previousDateURL: function () {
        var today = moment(this.terms.date);
        var daystoMonday = 0 - (1 - today.isoWeekday()) + 3;

        var lastWeek = today.subtract('days', daystoMonday);//MODIFMAX END 1 to daystoMonday

        return getDateURL(lastWeek, this.terms.category);
    },
    nextDateURL: function () {
        var currentDate = moment(this.terms.date);
        var newDate = currentDate.add(1, 'weeks');//MODIFMAX END weeks
            
        var futur = moment().startOf('week').day(5); // MODIFMAX END
        //var present = moment().add(1, 'days');
        
        //console.log(present.format("YYYY-MM-DD"), newDate.format("YYYY-MM-DD"));
        if(futur.format("YYYY-MM-DD") != newDate.format("YYYY-MM-DD")){
            return getDateURL(newDate, this.terms.category);
        }
        
    },
    previousDate: function () {
        var today = moment(this.terms.date);
        var daystoMonday = 0 - (1 - today.isoWeekday()) + 3;

        var lastWeek = today.subtract('days', daystoMonday);//MODIFMAX 1 to daystoMonday END
        return lastWeek.format("DD MMM");
    },
    nextDate: function () {
        var currentDate = moment(this.terms.date);
        var newDate = currentDate.add(1, 'weeks');//MODIFMAX weeks END
        return newDate.format("DD MMM");
    },
});

Meteor.startup(function () {
    // Search
    Template.search.helpers({
        canSearch: function () {
            if(Meteor.user()){
                return false;
            }else{
                return true;
            }
        }
      });
    
    //Show banner newsletter for always
    Template.newsletter_banner.helpers({
        showBanner: function () {
            if (Meteor.user() || Users.is.admin(Meteor.user())) {
                return false;
            } else {
                return true;
            }
        }
    });

    Router.route('not_found', {
        path: '/not_found'
    });


    if ( /*window.location.pathname == '/' || */ window.location.pathname == '/register' && !Users.is.admin(Meteor.user())) {
        Router.go('not_found');
    }
});

//Event for newslettr banner
Template.newsletter_banner.events({
    'click .newsletter-button': function (e) {
        e.preventDefault();
        var $banner = $('.newsletter-banner');
        if (Meteor.user()) {
            $banner.addClass('show-loader');
            Meteor.call('addCurrentUserToMailChimpListWithCategory', this.terms.category, function (error, result) {
                $banner.removeClass('show-loader');
                if (error) {
                    console.log(error);
                    Messages.flash(error.message, "error");
                } else {
                    console.log(result);
                    confirmSubscription();
                }
            });
        } else {
            var email = $('.newsletter-email').val();
            if (!email) {
                alert('Please fill in your email.');
                return;
            }
            $banner.addClass('show-loader');
            //change method meteor for add this category of the newsletter
            Meteor.call('addEmailToMailChimpListWithCategory', email, this.terms.category, function (error, result) {
                $banner.removeClass('show-loader');
                if (error) {
                    console.log(error);
                    Messages.flash(error.reason, "error");
                } else {
                    Messages.clearSeen();
                    console.log(result);
                    confirmSubscription();
                }
            });
        }
    }
});

// Register for the user who want an account
Template.new_register.events({
    'click .submit': function (e) {
        Meteor.call('addEmailToMailChimpForWaitRegister', $(".at-field-email"), function (error, result) {
            if (error) {
                console.log(error);
                Messages.flash(error.reason, "error");
            } else {
                Messages.clearSeen();
                console.log(result);
                Messages.flash("On vous enverra un mail de confirmation", "success");
            }
        });
    }
});

// Modify link url extern
Template.post_title.helpers({
    postLink: function () {
        return "/posts/" + this._id;
    },
    postTarget: function () {
        return '';
    }
});
