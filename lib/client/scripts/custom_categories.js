/*Add a field: listId for a list in mailchimp*/

Meteor.startup(function () {
    Categories.schema = new SimpleSchema({
        name: {
            type: String,
            editableBy: ["admin"]
        },
        description: {
            type: String,
            optional: true,
            editableBy: ["admin"],
            autoform: {
                rows: 3
            }
        },
        order: {
            type: Number,
            optional: true,
            editableBy: ["admin"]
        },
        slug: {
            type: String,
            optional: true,
            editableBy: ["admin"]
        },
        image: {
            type: String,
            optional: true,
            editableBy: ["admin"]
        },
        listId: {
            type: String,
            optional: true,
            editableBy: ["admin"]
        }
    });
    Categories.attachSchema(Categories.schema);
});