
Template.post_admin.events({
    /*On click on approve link, the post will be in wait*/
    'click .approveAndWait-link': function (e) {
        //count post between 2 dates
        Session.set('post', this);
        Meteor.call('findPostsBetweenDates', this.categories[0], function (err, result) {
            if (err) {
                console.log(err);
            } else {
                console.log(result);
                
/*                if (result < 3) {*/
                    Meteor.call('approveAndWaitPost', Session.get('post'));
                    e.preventDefault();
                /*} else {
                    //popup error
                    Messages.flash("C'est bon pour cette semaine, il y a le compte", "error");
                }*/
            }
        })
        

        e.preventDefault();
    },
    'click .rejected-link': function (e) {
        Meteor.call('rejectedPost', this);
    }
});


Template.post_admin.helpers({
    showUnapprove: function () {
        return this.status === Posts.config.STATUS_APPROVED || this.status === 4;
    },
    showRejected: function () {
        return this.status === Posts.config.STATUS_REJECTED;
    }
});