# telescope-theme-uzful

CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Modify
 * API KEY
 * TO DO
 
INTRODUCTION
------------
telescope-theme-uzful is a theme for telescope JS

https://github.com/TelescopeJS/Telescope

ARCHITECTURE
------------

i18n/ : contient des fichiers JSON de traduction
lib/client/uzful.js : Contrôleur, vue, routes
lib/client :
	scripts :
		custom_categories.js : Permet de modifier les champs de catégorie
		template.js : contient les setup des templates
	scss : styles
	templates : Tous les différents templates opérant sur le client sont dans ce dossiers
lib/posts : méthode pour les postes d'ajout
lib/server :
	templates : Templates opérant sur le serveur
lib/custom_permissions.js : Gestion des permissions
lib/custom_routes.js : Gestion des routes pour les posts
public/: dossier de ressources
packages.js : description du packages et configuration du comportement

LINK
-----

- /registernewusersuzful

=> Register new user

MODIFY
------

For modify a template:

  - Create a file with your template
  - Add Template.[name_of_your_template].replaces('[template_to_replace]') in templates.js
  - Add your file template in package.js
  
For modify a template handlebars (http://telescope.readme.io/v0.20/docs/custom-templates):

  - Create a file named custom_[name of file original]
  - Add your file template in package.js
  
For modify a helpers or events template, the syntax is:

Template.[name_template_original].helpers et Template.[name_template_original].events
  
API KEY
-------

MailChimp

 - id list : 206581
 - key api : cec77eb4f20d9bb600d334430b0bf279-us6
 
 TO DO
 -----
 
 
